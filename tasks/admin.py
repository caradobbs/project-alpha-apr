from django.contrib import admin
from tasks.models import Task

# Register your models here.


class ProjectAdmin(admin.ModelAdmin):
    pass


admin.site.register(Task, ProjectAdmin)
